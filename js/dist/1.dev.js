"use strict";

$(document).ready(function () {
  $("#Button_1").click(function () {
    $("#MyCarrussel").carousel("pause");
  });
  $("#Button_2").click(function () {
    $("#MyCarrussel").carousel("cycle");
  });
  $(".carousel-control-prev").click(function () {
    $("#MyCarrussel").carousel("prev");
  });
  $(".carousel-control-next").click(function () {
    $("#MyCarrussel").carousel("next");
  });
});